import TransactionsService.*
import com.example.api.SpringBootBoilerplateApplication
import com.google.gson.Gson
import io.grpc.ManagedChannelBuilder
import io.grpc.ServerBuilder
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import multipaxos.*
import org.apache.log4j.BasicConfigurator
import org.apache.zookeeper.data.Stat
import org.springframework.boot.runApplication
import zookeeper.kotlin.WatchersListImpl
import zookeeper.kotlin.ZKPaths
import zookeeper.kotlin.ZooKeeperKt
import zookeeper.kotlin.ZookeeperKtClient
import zookeeper.kotlin.createflags.Ephemeral
import zookeeper.kotlin.createflags.Persistent
import zookeeper.kotlin.createflags.Sequential
import java.io.PrintWriter
import java.io.StringWriter
import java.lang.Math.ceil
import java.math.BigInteger
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import java.util.stream.Collectors
import kotlin.time.*


suspend fun main(args: Array<String>) =  coroutineScope {

    // constants for the server's run
    val my_id : Int = args[0].toInt()
    val no_of_servers : Int = args[1].toInt()
    val no_of_shards = ceil(no_of_servers/3.0).toInt()
    val servers_in_my_shard = servers_in_my_shard(no_of_servers, no_of_shards, my_id)

//    while(true){
//        val serializator = Gson()
//        val y = serializator.toJson(Operation(mutableListOf(),false,BigInteger("5"),0,0))
//        println(y)
//        val z = y.toByteArray()
//        println(z)
//        println(String(z,Charsets.UTF_8))
//        val x : Operation = serializator.fromJson(String(z,Charsets.UTF_8),Operation::class.java)
//        println(x.rest_channel_id)
//        //Json.encodeToString(Operation(mutableListOf(),false,BigInteger("5"),0,0)).toByteStringUtf8()
//    }



    // a gson serializator
    val serializator = Gson()

    // map for tx_id -> channel - needed for noatifying the rest thread the output
    val rest_channels = ConcurrentHashMap<BigInteger, Channel<RestResult>>()

    // start zookeeper server
    BasicConfigurator.configure()

    val zkSockets = (1..1).map { Pair("127.0.0.1", 2180 + it) }

    val zkConnectionString = makeConnectionString(zkSockets)

    withZooKeeper(zkConnectionString) {
        val zk = it

        // starting worker channel
        val operations_channel = Channel<Operation>(Channel.UNLIMITED)

        val ab_channel = Channel<Operation>(Channel.UNLIMITED)

        //initiating server data - with Genesis UTxO
        val server_data = ServerData()
        if(getMyShard(my_id) == getAccountShard(BigInteger("0"),no_of_shards)){
            val utxo = uTxO{
                txId = "0"
                address = "0"
                amount = 9223372036854775807
                timestamp = "1"
            }
            server_data.addUTxO(utxo)
        }

        var timestamp = (getMyShard(my_id) + 2*no_of_shards).toBigInteger()

        var current_atl_id = BigInteger("0")

        //start grpc server
        val service = ServerServices(server_data,ab_channel,rest_channels,my_id,50000+my_id)
        val server = ServerBuilder.forPort(50000+my_id).addService(service).build()

        withContext(Dispatchers.IO) {
            server.start()
        }

        var stubMap : Map<Int,ServerServicesGrpcKt.ServerServicesCoroutineStub> = mutableMapOf()
        for (i in 1..no_of_servers){
            stubMap = stubMap + Pair(i,ServerServicesGrpcKt.ServerServicesCoroutineStub
            (ManagedChannelBuilder.forAddress("localhost",50000+i).usePlaintext().build()!!))
        }


        try {
            deleteRecursive(zk,"/ATLs")
        } catch (e: Exception) {
        }

        val myOmegaMap : HashMap<Int,OmegaFailureDetector<Int>> = HashMap()


        for (i in 1..no_of_shards){
            if(i == getMyShard(my_id)){
                val myOmega = Omega.make(zk,i,my_id)
                myOmega.register()
                myOmegaMap[i] = myOmega
            }
            else{
                myOmegaMap[i] = Omega.make(zk,i,my_id)
            }
        }

        while(zk.getChildren("/omegas").first.size!= no_of_shards){
            delay(100L)
        }
        while(true){
            delay(100L)
            var counter : Int = 0
            for(i in 1..no_of_shards){
                if(zk.getChildren("/omegas/sh$i").first.size==3)
                    counter++
            }
            if(counter==no_of_shards)
                break
        }



        try {
            zk.create("/ATLs") {
                flags = Persistent
            }
        } catch (e: Exception) {

        }



        SpringBootBoilerplateApplication.my_server_id = my_id
        SpringBootBoilerplateApplication.stubMap = stubMap
        SpringBootBoilerplateApplication.omegaMap = myOmegaMap

        runApplication<SpringBootBoilerplateApplication>(*args)

        //atomic broadcast
        launch{
            ab_main(my_id, no_of_servers, getMyShard(my_id), zk , ab_channel, operations_channel, rest_channels)
        }



        while(true){ //worker
            println("worker - waiting for a new op with timestamp = $timestamp")
            val op = operations_channel.receive()
            println("Worker Received an operation")
            if(op.is_sent){
                if(op.tx_list.isEmpty()){
                    if(op.distributer_server != my_id){
                        println("History - I'm not the distributer server so bye...")
                        continue
                    }


                    val initialTime = System.currentTimeMillis();
                    val shardsListMap : HashMap<Int,List<Tx>> = HashMap()

                    var isDone: ConcurrentHashMap<Int, Deferred<Unit>> = ConcurrentHashMap()
                    println("WORKER: History - sending each shard now")
                    for(shard in 1..no_of_shards){
                        if(shard == getMyShard(my_id)){
                            continue
                        }
                        isDone[shard] =  async{
                            var omegaInShard = myOmegaMap[shard]?.leader

                            var response:  GetTransactionsHistoryResponse? = null

                            val request = getTransactionsHistoryRequest{
                                this.limit = op.limit
                            }

                            var failed = false
                            while(true){
                                val currentTime = System.currentTimeMillis();
                                try{
                                    response = stubMap[omegaInShard]?.withDeadlineAfter(10, TimeUnit.SECONDS)
                                            ?.receiveGetTransactionHistory(request)
                                    if (response != null) {
                                        shardsListMap[shard] = response.historyList
                                    }
                                    failed = false
                                }
                                catch(e: Exception){
                                    failed = true
                                }

                                if((!failed && response != null) || (currentTime - initialTime)/1000 > 10){
                                    break;
                                }
                                omegaInShard = myOmegaMap[shard]?.leader
                            }
                        }
                    }

                    println("WORKER: History - sent")
                    ///waiting for the timeout / success of every shard
                    for(shard in 1..no_of_shards){
                        if(shard == getMyShard(my_id)){
                            continue
                        }
                        isDone[shard]?.await()
                    }
                    println("WORKER: History - waited")


                    if(shardsListMap.size != no_of_shards-1){
                        rest_channels[op.rest_channel_id]?.send(RestResult(false,"TIMEOUT"))
                        continue
                    }
                    println("WORKER: History - everyone sent")

                    var mergedList : MutableList<Tx> = server_data.getLocalTxSortedByTimestamp()
                    println("WORKER: History - after getLocalTx")
                    for((_,shardList) in shardsListMap){ // note that those lists are not distinct and the distinction happens inside Rest
                        if(op.limit.toLong() == (-1).toLong()){
                            mergedList = java.util.stream.Stream.concat(mergedList.stream(), shardList.stream())
                                    .sorted { o1: Tx, o2: Tx -> BigInteger(o2.timestamp).compareTo(BigInteger(o1.timestamp)) }.collect(Collectors.toList())
                        }
                        else{
                            mergedList = java.util.stream.Stream.concat(mergedList.stream(), shardList.stream())
                                    .sorted { o1: Tx, o2: Tx ->BigInteger(o2.timestamp).compareTo(BigInteger(o1.timestamp)) }.limit(op.limit.toLong()).collect(Collectors.toList())
                        }
                    }
                    println("WORKER: sending rest channel")
                    rest_channels[op.rest_channel_id]?.send(RestResult(true,"",mergedList))
                    println("WORKER: Finished History")
                    continue
                }
                if(op.tx_list.size > 1){
                    var maxTimestamp : BigInteger = timestamp
                    var atl_id_looked_at = current_atl_id
                    println("I got a list!")

                    println("WORKER: listexecuted size: " + server_data.executedListHash.size)
                    println(server_data.listExecuted(op.tx_list))
                    if(server_data.listExecuted(op.tx_list)){ //if already done
                        println("WORKER: List already done")
                        SendResult(rest_channels, my_id, op, RestResult(false, "Already done"))
                        continue
                    }
                    // Only "true responsible" shard: opens up ATL node, and send all relavent shards
                    if(getMyShard(my_id) == getAccountShard(BigInteger(op.tx_list[0].inputsList[0].address), no_of_shards)){
                        println("I am the responsible shard")
                        var existsAttemptCounter = 0

                        while(true){ //waiting until the current list's ATL node is open
                            println("THIS IS THE ATTEMPT NUMBER $existsAttemptCounter")
                            println("WORKER - BEFORE LEADER CHECK")
                            println(myOmegaMap[getMyShard(my_id)]!!.leader)
                            println("WORKER - AFTER LEADER CHECK")
                            if(my_id == myOmegaMap[getMyShard(my_id)]!!.leader){
                                println("I am the leader, opening up the ATL node")

                                atl_id_looked_at = BigInteger(zk.create("/ATLs/ATL-") {
                                    flags = Persistent and Sequential
                                    data = serializator.toJson(op.tx_list).toByteArray()
                                }.first.let { ZKPaths.extractSequentialSuffix(it)!! })
                                println("after creation of the ATL node")
                                break
                            }
                            try {
                                //if (zk.getChildren("/ATLs").first.size >= atl_id_looked_at.toInt()+1){
                                if (existsRecursive(zk, "/ATLs/ATL-"+String.format("%010d",atl_id_looked_at)).second != null) {
                                    val atl_data: ByteArray =
                                            zk.getData("/ATLs/ATL-"+String.format("%010d",atl_id_looked_at), false, Stat()).first
                                    val atlList: List<Tx> =
                                            serializator.fromJson(String(atl_data, Charsets.UTF_8), Array<Tx>::class.java)
                                                    .toList()
                                    if (atlList.equals(op.tx_list)) { //checking if it is our list
                                        break
                                    }
                                    atl_id_looked_at = atl_id_looked_at.add(BigInteger("1"))
                                }
                                else{
                                    existsAttemptCounter++

                                }
                            } catch (e: Exception) {
                                println("WORKER - EXCEPTION CAUGHT FOR EXISTS")
                            }
                        }
                        println("WORKER - OUT OF EXISTS")
                        val relevantShards : HashMap<Int,Int> = HashMap()

                        for(tx in op.tx_list){
                            for(utxo in tx.inputsList) {
                                val accountID = BigInteger(utxo.address)
                                val accountShard = getAccountShard(accountID, no_of_shards)
                                if (accountShard != getMyShard(my_id)) {
                                    relevantShards[accountShard] = 0
                                }
                            }
                        }

                        println("WORKER: relaventShards size - " + relevantShards.size)
                        for((shard,_) in relevantShards){
                            launch{
                                var omegaInShard = myOmegaMap[shard]?.leader

                                var response:  CreateTxFromListResponse? = null

                                val finalL = createTxFromListRequest{
                                    list = txList{
                                        this.list += op.tx_list
                                    }
                                }

                                var failed = false

                                while(true){
                                    try{
                                        response =  stubMap[omegaInShard]?.createTxFromList(finalL)
                                        failed = false
                                    }
                                    catch(e: Exception){
                                        failed = true
                                    }

                                    if(!failed &&  response != null){
                                        break;
                                    }
                                    omegaInShard = myOmegaMap[shard]?.leader
                                }
                            }
                        }
                    }

                    println("WORKER: going over all the atls")
                    // ATL node is open, now move over all ATL nodes until this one (included)
                    while(true){
                        val ATLID = String.format("%010d",current_atl_id)
                        val myShard = getMyShard(my_id)
                        val atl_id_path = "/ATLs/ATL-"+ATLID
                        val participantsShards : HashMap<Int,Int> = HashMap()


                        val currentList: List<Tx> =  serializator.fromJson(String(zk.getData(atl_id_path,false, Stat()).first,Charsets.UTF_8),Array<Tx>::class.java).toList()
                        var responsibility = false
                        var is_valid = true
                        println("WORKER: list - checking if list is valid")
                        for(tx in currentList){
                            participantsShards[getAccountShard(BigInteger(tx.inputsList[0].address),no_of_shards)] = 0
                            if(getAccountShard(BigInteger(tx.inputsList[0].address),no_of_shards)==getMyShard(my_id)){
                                responsibility = true
                                if(TxUtxoNotValid(tx,server_data) || TxAlreadyDone(tx,server_data)){ //if not valid
                                    is_valid = false
                                    break
                                }
                            }
                        }
                        println("WORKER: list - checked if list is valid")

                        if(responsibility == false){ // Just Move on
                            println("WORKER: list - not my responsibility")
                            current_atl_id = current_atl_id.add(BigInteger("1"))
                            continue
                        }

                        if(is_valid == false){
                            println("WORKER - list NOT VALID")
                            try{ // decline
                                zk.create {
                                    path = "/ATLs/ATL-$ATLID/sh-$myShard"
                                    flags = Persistent
                                    data = BigInteger("0").toByteArray()
                                }
                            }
                            catch (e : Exception){
                            }
                            current_atl_id = current_atl_id.add(BigInteger.ONE)
                            if(currentList.equals(op.tx_list)){ //We just did our List (from the queue)
                                SendResult(rest_channels, my_id, op, RestResult(false, "List with not valid Tx"))
                                break
                            }
                            continue
                        }

                        try{ //accept
                            zk.create {
                                path = "/ATLs/ATL-$ATLID/sh-$myShard"
                                flags = Persistent
                                data = timestamp.toByteArray()
                            }
                        }
                        catch (e : Exception){
//                            val sw = StringWriter()
//                            val pw = PrintWriter(sw)
//                            e.printStackTrace(pw)
//                            val sStackTrace = sw.toString() // stack trace as a string
//
//                            println(sStackTrace)
                        }
                        //check shard results
                        maxTimestamp = timestamp
                        var rejected = false
                        while(true){
                            if(zk.getChildren("/ATLs/ATL-$ATLID").first.size != participantsShards.size) {
                                delay(100L)
                                continue;
                            }
                            else{
                                for(sh in participantsShards.keys){
                                    zk.getData(atl_id_path+"/sh-$sh", false, Stat())
                                    val sh_timestamp = BigInteger(zk.getData(atl_id_path+"/sh-$sh", false, Stat()).first)
                                    if(sh_timestamp.equals(BigInteger("0"))){
                                        rejected = true
                                        break
                                    }
                                    maxTimestamp = maxTimestamp.max(sh_timestamp)
                                }
                                break
                            }

                        }
                        // wait(how?) until all shards accepted (if one rejected then reject too and pass)
                        // if all accepted, take maximum timestamp
                        if(rejected) { //if not all accepted
                            current_atl_id = current_atl_id.add(BigInteger.ONE)
                            if(currentList.equals(op.tx_list)){ //We just did our List (from the queue)
                                SendResult(rest_channels, my_id, op, RestResult(false, "List was rejected"))
                                break
                            }
                            continue
                        }
                        timestamp = maxTimestamp.add((BigInteger((getMyShard(my_id)+no_of_shards).toString()) - maxTimestamp.mod(BigInteger(no_of_shards.toString())) ))
                        timestamp = timestamp.add(no_of_shards.toBigInteger())

                        server_data.executedListHash[currentList]=0

                        val receiverMap : HashMap<Int,Int> = HashMap()
                        val updatedList : MutableList<Tx> = mutableListOf()
                        for (tx in currentList){
                            val updatedTx = txSetTimestamp(tx,maxTimestamp)
                            updatedList.add(updatedTx)
                            server_data.addSentTx(updatedTx)
                            for(utxo in updatedTx.inputsList){
                                server_data.removeUTxO(utxo)
                            }
                            val outputs = updatedTx.outputsList


                            for(tr in outputs){
                                val accountId = BigInteger(tr.address)
                                if(isAccountInShard(accountId,my_id,no_of_shards)){
                                    val utxo = uTxO{
                                        txId = updatedTx.txId
                                        address = tr.address
                                        amount = tr.amount
                                        this.timestamp = updatedTx.timestamp
                                    }
                                    server_data.addRecivedTx(updatedTx, BigInteger(tr.address)) //order is critical
                                    server_data.addUTxO(utxo)                                   //order is critical
                                }
                                else{
                                    val shard = getAccountShard(accountId,no_of_shards)
                                    if(shard !in participantsShards){
                                        receiverMap[shard] = 0;
                                    }
                                }
                            }

                        }
                        //  your shard is the one that has it currently in the queue(the first Tx's shard's
                        if(getAccountShard(BigInteger(currentList[0].inputsList[0].address),no_of_shards)==getMyShard(my_id)){
                            for((shard,_) in receiverMap) {
                                launch{
                                    var omegaInShard = myOmegaMap[shard]?.leader

                                    var response:  CreateTxFromListResponse? = null

                                    val finalL = createTxFromListRequest{
                                        list = txList{
                                            this.list += updatedList
                                        }
                                    }

                                    var failed = false

                                    while(true){
                                        try{
                                            response =  stubMap[omegaInShard]?.receiveTxFromList(finalL)
                                            failed = false
                                        }
                                        catch(e: Exception){
                                            failed = true
                                        }

                                        if(!failed && response != null){
                                            break;
                                        }
                                        omegaInShard = myOmegaMap[shard]?.leader
                                    }
                                }
                            }
                        }
                        current_atl_id = current_atl_id.add(BigInteger("1"))
                        if(currentList.equals(op.tx_list)){ //We just did our List (from the queue)
                            break
                        }
                    } // end of doing all Atomic lists needed
                    // Sending the result to rest channel
                    println("worker - in 5")
                    SendResult(rest_channels, my_id, op, RestResult(true, maxTimestamp.toString()))
                    continue
                }
                else{ //only 1 tx
                    var tx: Tx = op.tx_list[0]
                    println("Operation is_sent and is only 1 Tx")


                    //checking if transaction was already sent
                    if(TxAlreadyDone(tx, server_data)){
                        println("Transaction Already Sent")
                        SendResult(rest_channels, my_id, op, RestResult(false, "Transaction Already Sent - ${tx.timestamp}"))
                        continue; // Move to new operataion
                    }
                    //checking if all UTxO exists

                    if(TxUtxoNotValid(tx, server_data)){
                        println("UTxO Used Twice")
                        SendResult(rest_channels, my_id, op, RestResult(false, "UTxO Used Twice"))
                        continue; // move to new Operation
                    }
                    /////// Tx is valid
                    // timstamp corrent transaction
                    tx = txSetTimestamp(tx, timestamp)

                    println("WORKER: Transaction timestamp:" +tx.timestamp)

                    //update timestamp
                    timestamp = timestamp.add(no_of_shards.toBigInteger())
                    //updating data structure
                    server_data.addSentTx(tx)
                    for(utxo in tx.inputsList){
                        println("worker - removing utxo")
                        server_data.removeUTxO(utxo)
                    }
                    val outputs = tx.outputsList

                    val receiverMap : HashMap<Int,Int> = HashMap()

                    for(tr in outputs){
                        val accountId = BigInteger(tr.address)
                        if(isAccountInShard(accountId,my_id,no_of_shards)){
                            val utxo = uTxO{
                                txId = tx.txId
                                address = tr.address
                                amount = tr.amount
                                this.timestamp = tx.timestamp
                            }
                            server_data.addRecivedTx(tx, BigInteger(tr.address)) //order is critical
                            server_data.addUTxO(utxo) //order is critical

                        }
                        else {
                            val shard = getAccountShard(accountId,no_of_shards)
                            receiverMap[shard] = 0;
                        }
                    }
                    for((shard,_) in receiverMap) {
                        println("WORKER: sending shards")
                        launch{
                            var omegaInShard = myOmegaMap[shard]?.leader

                            var response:  CreateTxFromListResponse? = null

                            val finalL = createTxFromListRequest{
                                list = txList{
                                    this.list += mutableListOf<Tx>(tx)
                                }
                            }

                            var failed = false

                            while(true){
                                try{
                                    response =  stubMap[omegaInShard]?.receiveTxFromList(finalL)
                                    failed = false
                                }
                                catch(e: Exception){
                                    failed = true
                                }

                                if(!failed && response != null){
                                    break;
                                }
                                omegaInShard = myOmegaMap[shard]?.leader
                            }
                        }
                    }
                    println("worker - in 7")
                    SendResult(rest_channels, my_id, op, RestResult(true, tx.timestamp))
                    continue
                }

            }
            else{ // getting money
                println("WORKER - Got some Moneyyyyy")
                if(op.tx_list.isEmpty()){
                    if(my_id==op.distributer_server){
                        println("Sending the needed rest channel...")
                        rest_channels[op.rest_channel_id]?.send(RestResult(true,"",server_data.getLocalTxSortedByTimestamp()))
                    }
                    continue
                }
                //update timestamp
                timestamp = timestamp.max(BigInteger(op.tx_list[0].timestamp))
                // making sure timestamp % no_of_shards = Myshard
                timestamp = timestamp.add((BigInteger((getMyShard(my_id)+no_of_shards).toString()) - timestamp.mod(BigInteger(no_of_shards.toString())) ))
                if(server_data.receivedHashByTxId.containsKey(BigInteger(op.tx_list[0].txId))){
                    println("worker - in 8")
                    SendResult(rest_channels, my_id, op, RestResult(false, "Transaction Already Sent"))
                    continue; // Move to new operataion
                }
                for(tx in op.tx_list){
                    //updating data structure
                    val outputs = tx.outputsList
                    for(tr in outputs){ // only if the client gets money add it...
                        val accountId = BigInteger(tr.address)
                        if(isAccountInShard(accountId,my_id,no_of_shards)){
                            val utxo = uTxO{
                                this.txId = tx.txId
                                this.address = tr.address
                                this.amount = tr.amount
                                this.timestamp = tx.timestamp
                            }
                            server_data.addRecivedTx(tx, accountId) //order is critical
                            server_data.addUTxO(utxo) //order is critical
                        }
                    }
                }
            }
            println("worker - in 9")
            SendResult(rest_channels, my_id, op, RestResult(true, ""))
        } // end of while

    }
}

fun TxAlreadyDone(tx: Tx, server_data: ServerData): Boolean{
    //checking if transaction was already sent
    val tx_id = BigInteger(tx.txId)
    return server_data.sentHashByTxId.containsKey(tx_id)
}

fun TxUtxoNotValid(tx: Tx, server_data: ServerData): Boolean{ // if true it is bad...
    val utxoList = tx.inputsList
    var utxoNotFound = false
    var input_sum: Long = 0
    var output_sum: Long = 0
    for(utxo in utxoList){
        if(server_data.checkUTxOValid(utxo) && (server_data.isUTxOInFuture(utxo) || BigInteger(utxo.txId) in server_data.receivedHashByTxId.keys || BigInteger(utxo.txId) in server_data.sentHashByTxId.keys)){
            utxoNotFound = true;
            break;
        }
        input_sum += utxo.amount
    }
    for(tr in tx.outputsList){
        output_sum += tr.amount
    }
    return utxoNotFound || input_sum != output_sum
}


suspend fun SendResult(rest_channels: ConcurrentHashMap<BigInteger, Channel<RestResult>>, my_id: Int, op: Operation, rest_result: RestResult) {
    if(op.distributer_server == my_id){ // check if I'm Omega
        // send rest Server result
        println("Worker - Sending timestamp - ${rest_result.msg}")
        GlobalScope.launch {
            rest_channels[op.rest_channel_id]?.send(rest_result)
        }

    }
}

fun servers_in_my_shard(no_of_servers:Int, no_of_shards:Int, my_id: Int): List<Int>{
    if(my_id % 3 == 0){
        return listOf(my_id-2,my_id-1,my_id)
    }
    else if (my_id % 3 == 1){
        return listOf(my_id,my_id+1,my_id+2)
    }
    return listOf(my_id-1,my_id,my_id+1)
}

fun getMyShard(myID : Int) : Int{
    val res = ceil(myID / 3.0)
    return res.toInt()
}

fun getAccountShard(accountId: BigInteger,noOfShards: Int) : Int {
    return accountId.mod(noOfShards.toBigInteger()).toInt() + 1
}

fun isAccountInShard(accountId: BigInteger, my_id: Int, noOfShards:Int): Boolean{
    return getMyShard(my_id) == getAccountShard(accountId,noOfShards)
}

fun txSetTimestamp(tx:Tx, timestamp: BigInteger): Tx{
    return tx{
        txId = tx.txId
        inputs += tx.inputsList
        outputs += tx.outputsList
        this.timestamp = timestamp.toString()
    }
}



class Membership private constructor(private val zk: ZooKeeperKt, val groupName: String) {
    var _id: String? = null
    val id: String get() = _id!!

    var onChange: (suspend () -> Unit)? = null

    suspend fun join(id: String) {
        val (_, stat) = zk.create {
            path = "/$id"
            flags = Ephemeral
        }
        _id = id
    }

    suspend fun queryMembers(): List<String> = zk.getChildren("/") {
        watchers += this@Membership.onChange?.let { { _, _, _ -> it() } }
    }.first

    suspend fun leave() {
        zk.delete("/$id")
    }

    companion object {
        suspend fun make(zk: ZooKeeperKt, groupName: String): Membership {
            val zk = zk.usingNamespace("/membership")
                    .usingNamespace("/$groupName")
            return Membership(zk, groupName)
        }
    }
}

suspend fun deleteRecursive(zk : ZooKeeperKt, absolutePath : String){
    if(zk.getChildren(absolutePath).first.size==0){
        zk.delete(absolutePath)
        return
    }
    for(relativeChildPath in zk.getChildren(absolutePath).first){
        val absoluteChildPath = absolutePath + "/" + relativeChildPath
        deleteRecursive(zk,absoluteChildPath)
    }
    zk.delete(absolutePath)
}

suspend fun existsRecursive(zk : ZooKeeperKt, absolutePath: String) : Pair<Boolean,Stat?>  {
    try{
        val x : Pair<Boolean,Stat?> = zk.exists(absolutePath)
        return x
    }
    catch(e : Exception){
        println("existsRecursiveGotException")
//        val sw = StringWriter()
//        val pw = PrintWriter(sw)
//        e.printStackTrace(pw)
//        val sStackTrace = sw.toString() // stack trace as a string
//
//        println(sStackTrace)
        return Pair(false,null)
    }
}


suspend fun recursiveCreate(zk : ZooKeeperKt, shardNo : Int){
    try{
        zk.create("/omegas"){
            flags = Persistent
        }
        zk.create("/omegas/sh$shardNo"){
            flags = Persistent
        }
    }
    catch (e : Exception){
        try{
            zk.create("/omegas/sh$shardNo"){
                flags = Persistent
            }
        }
        catch(e : Exception){

        }
    }
}