//import kotlinx.cli.*

import TransactionsService.*
import com.example.api.TrForTxWrapper
import com.example.api.TrWrapper
import com.example.api.TxWrapper
import com.example.api.UTxOWrapper
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.springframework.http.*
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import java.io.*
import java.math.BigInteger
import java.security.*
import java.util.*


class Client(public val accountID: BigInteger, public val secretKey: BigInteger){

}
val ctrLock : Mutex = Mutex()

var clients: HashMap<BigInteger,Client> = HashMap()
var txCtr = 0
var localBuiltTx: HashMap<BigInteger, Tx> = HashMap()
var listCtr = 0
var localBuiltList: HashMap<Int, TxList> = HashMap()

fun getAccountsTest(){
    val restTemplate = RestTemplate()
    val resource_uri = "http://localhost:8080/test"
//    val response = restTemplate.getForEntity(resource_uri, UTxOWrapper::class.java)
    val utxo: UTxOWrapper = restTemplate.getForObject(resource_uri, UTxOWrapper::class.java)!!
    println(utxo.amount)
}


fun isNewClient(str : String) : Boolean{
    return str.startsWith("NewClient",true)
}

fun isGetAllClients(str : String) : Boolean{
    return str.startsWith("GetAllClients",true)

}

fun isGetAllTxIds(str: String) : Boolean{
    return str.startsWith("GetAllTxIds",true)
}

fun isGetAllListIds(str: String) : Boolean{
    return str.startsWith("GetAllAtomicListIds",true)

}

fun isBuildTx(str: String) : Boolean{
    return str.startsWith("BuildTx",true)
}

fun isAtomicList(str: String) : Boolean{
    return str.startsWith("BuildList",true)
}

fun isSendTx(str: String) : Boolean{
    return str.startsWith("SendTx",true)
}

fun isSendTr(str : String) : Boolean{
    return str.startsWith("SendTr",true)
}

fun isSendList(str: String) : Boolean{
    return str.startsWith("SendList",true)
}

fun isGetUnspent(str: String) : Boolean{
    return str.startsWith("GetUnspent",true)
}

fun isGetHistory(str : String) : Boolean{
    return str.startsWith("GetHistory",true)
}

fun isGetAllTxs(str: String) : Boolean{
    return str.startsWith("GetAllTxs",true)
}

fun executeNewClient(){
    // drawing a 128bit id for the channel
    val rn = Random();
    val clientID = BigInteger(128, rn)
    clients[clientID] = Client(clientID, clientID)
    println("Client " + clientID.toString() + " created")
}

fun executeGetAllClients(){
    println("Clients:")
    for(clientID in clients.keys){
        println(clientID.toString())
    }
}

fun executeGetAllTxIds(){
    println("Transactions:")
    for(txID in localBuiltTx.keys){
        println(txID.toString())
    }
}

fun executeGetAllListIds(){
    println("Lists:")
    for(listID in localBuiltList.keys){
        println(listID.toString())
    }
}

suspend fun executeBuildTx(str : String): BigInteger{
    var senderID : BigInteger = BigInteger(("-1"))
    val utxoList : MutableList<UTxO> = mutableListOf()
    val transferList: MutableList<Tr> = mutableListOf()
    try {
        //Parsing...
        val splittedString: List<String> = str.split(" ")
        //Get ID of sender
        senderID = splittedString[1].toBigInteger()
        //Get UTxOs
        val rawInputList: List<String> = splittedString[2].replace("{", "").replace("}", "")
                .replace("(", "").replace(")", "").split(",")
        val txIDsInputList: MutableList<BigInteger> = mutableListOf()
        val addressesInputList: MutableList<BigInteger> = mutableListOf()
        val amountInputList: MutableList<Long> = mutableListOf()


        for (index in 0 until rawInputList.size / 4) {
            utxoList.add(uTxO {
                this.txId = rawInputList[4 * index]
                this.address = rawInputList[4 * index + 1]
                this.amount = rawInputList[4 * index + 2].toLong()
                this.timestamp = rawInputList[4 * index + 3]
            })
        }
        //Get outputs
        val rawOutputList: List<String> = splittedString[3].replace("{", "").replace("}", "")
                .replace("(", "").replace(")", "").split(",")
        val addressesOutputList: MutableList<BigInteger> = mutableListOf()
        val amountOutputList: MutableList<Long> = mutableListOf()
        for (index in 0 until rawOutputList.size / 2) {
            addressesOutputList.add(rawOutputList[2 * index].toBigInteger())
            amountOutputList.add(rawOutputList[2 * index + 1].toLong())
            transferList.add(tr {
                this.amount = rawOutputList[2 * index + 1].toLong()
                this.address = rawOutputList[2 * index]
            })
        }
    }
    catch(e : Exception){
        println("PARSING ERROR - please insert the instruction again")
        return BigInteger("-1")
    }

    // build a Tx id
    val string: String
    ctrLock.withLock{
        string = senderID.toString() + txCtr.toString()
        txCtr++
    }

    val bytesOftx: ByteArray = string.toByteArray()
    val md = MessageDigest.getInstance("MD5")
    val theMD5digest = md.digest(bytesOftx)
    val txID: BigInteger = BigInteger(theMD5digest)

    val new_tx = tx{this.txId=txID.toString()
        this.inputs+=utxoList
        this.outputs+=transferList
        this.timestamp="0"}
    localBuiltTx[txID] = new_tx
    println("New Tx created with ID - $txID")
    return txID
}

suspend fun executeBuildAtomicList(str: String): Int{
    try{
        val txIDsList: List<BigInteger> = str.split(" ")[1].replace("{","").replace("}","")
                .split(",").map{it.toBigInteger()}
        val string: String
        val listID: Int
        ctrLock.withLock{
            listID = listCtr++
        }
        var list = mutableListOf<Tx>()
        for(txID in txIDsList){
            list.add(localBuiltTx[txID]!!)
        }
        localBuiltList[listID] = txList{this.list+=list}
        println("New Atomic List created with ID - $listID")
        return listID
    }
    catch(e: Exception){
        println("Error: bad txID")
        return -1
    }
}

fun executeSendTx(str: String): String{
    println("SendTx")
    var serverID : Int = -1
    var txID : BigInteger = BigInteger("-1")
    try {
        txID = str.split(" ")[1].toBigInteger()
        if (str.split(" ").size > 2) {
            serverID = str.split(" ")[2].removePrefix("-s=").toInt()
        }
        if (serverID == -1) {
            serverID = (1..noOfServers).random()
        }
    }
    catch(e: Exception){
        println("PARSING ERROR - please insert the instruction again")
        return ""
    }
    var myTx: Tx = tx{}

    try{
        myTx = localBuiltTx[txID]!!
    }
    catch(e:Exception){
        println("Error: bad txID")
        return ""
    }
    val accountID : BigInteger = myTx.inputsList[0].address.toBigInteger()
    val restTemplate = RestTemplate()
    val port = (8080 + serverID)
    val wrappedObject : TxWrapper = TxWrapper(myTx.txId,
            myTx.timestamp,
            myTx.inputsList.map{UTxOWrapper(it!!.txId,it!!.address,it!!.amount, it!!.timestamp)},
            myTx.outputsList.map{ TrWrapper(it!!.address,it!!.amount) })

    var responseEntity: ResponseEntity<String>? = null

    try{
        val requestBody = ""
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val param: MutableMap<String, String> = HashMap()
        param["accountID"] = "$accountID"
        param["txID"] = "$txID"
        val requestEntity: HttpEntity<TxWrapper> = HttpEntity<TxWrapper>(wrappedObject, headers)


        responseEntity = restTemplate.exchange("http://localhost:$port/accounts/{accountID}/transactions/{txID}", HttpMethod.PUT,requestEntity,String::class.java , param)
    }
    catch(e: HttpClientErrorException){
        println("Error: code " + e.statusCode.toString())
        println(e.message?.split(",")?.get(3)?.removePrefix("\"message\":"))
        if(e.message?.split(",")?.get(3)?.removePrefix("\"message\":")?.startsWith("\"Transaction Already Sent") == false){
            return ""
        }
        return  e.message?.split(" - ")?.get(1)!!
    }
    return responseEntity?.body!!
}

suspend fun executeSendTr(str: String): Pair<BigInteger, String>{
    println("SendTr")
    var serverID: Int = -1
    var senderID: BigInteger = BigInteger("-1")
    var address : BigInteger = BigInteger("-1")
    var amount : Long = -1
    try {
        senderID = str.split(" ")[1].toBigInteger()
        address = str.split(" ")[2].replace("(", "").replace(")", "").split(",")[0].toBigInteger()
        amount = str.split(" ")[2].replace("(", "").replace(")", "").split(",")[1].toLong()
        if (str.split(" ").size > 3) {
            serverID = str.split(" ")[3].removePrefix("-s=").toInt()
        }
        if (serverID == -1) {
            serverID = (1..noOfServers).random()
        }
    }
    catch(e: Exception){
        println("PARSING ERROR - please insert the instruction again")
        return Pair(BigInteger("-1"), "")
    }
    val restTemplate = RestTemplate()
    val port = (8080 + serverID)

    val string: String
    ctrLock.withLock{
        string = senderID.toString() + txCtr.toString()
        txCtr++
    }

    val bytesOftx: ByteArray = string.toByteArray()
    val md = MessageDigest.getInstance("MD5")
    val theMD5digest = md.digest(bytesOftx)
    val txID: BigInteger = BigInteger(theMD5digest)


    val objectBody : Pair<TrForTxWrapper,List<TxWrapper>?> = Pair(TrForTxWrapper(txID.toString(),address.toString(),amount),
            mutableListOf()
    )
    var timestamp : String? = ""
    try{
        timestamp = restTemplate.postForObject("http://localhost:$port/accounts/$senderID/transactions",objectBody,String::class.java)
    }
    catch(e: HttpClientErrorException){
        println("Error: code " + e.statusCode.toString())
        println(e.message?.split(",")?.get(3)?.removePrefix("\"message\":"))
        if(e.message?.split(",")?.get(3)?.removePrefix("\"message\":")?.startsWith("\"Transaction Already Sent") == false){
            return Pair(txID, "0")
        }
        timestamp = e.message?.split(" - ")?.get(1)
    }
    println("TxID = " + txID)
    println("timestamp = " + timestamp)
    return Pair(txID, timestamp!!)
}

fun executeSendList(str: String): String{
    println("SendList")
    var serverID: Int = -1
    var listID : Int = -1
    var senderID : BigInteger = BigInteger("-1")
    try {
        senderID = str.split(" ")[1].toBigInteger()
        listID = str.split(" ")[2].toInt()
        if (str.split(" ").size > 3) {
            serverID = str.split(" ")[3].removePrefix("-s=").toInt()
        }
        if (serverID == -1) {
            serverID = (1..noOfServers).random()
        }
    }
    catch(e: Exception){
        println("PARSING ERROR - please insert the instruction again")
        return ""
    }
    val restTemplate = RestTemplate()
    val port = (8080 + serverID)


    var objectBody : Pair<TrForTxWrapper,List<TxWrapper>?> = Pair(TrForTxWrapper("","",32), mutableListOf()) // garbage
    try{
        objectBody = Pair(TrForTxWrapper("","",32), //These are garbage values
                localBuiltList[listID]?.listList?.map {
                    TxWrapper(it.txId,
                            it.timestamp,
                            it.inputsList.map{
                                UTxOWrapper(it.txId,it.address,it.amount, it.timestamp)
                            },
                            it.outputsList.map{
                                TrWrapper(it.address,it.amount)
                            })
                }
        )
    }
    catch(e: Exception){
        println("Error: bad listID")
    }
    var timestamp: String? = ""
    try{
        timestamp = restTemplate.postForObject("http://localhost:$port/transactions",objectBody,String::class.java)
    }
    catch(e: HttpClientErrorException){
        println("Error: code " + e.statusCode.toString())
        println(e.message?.split(",")?.get(3)?.removePrefix("\"message\":"))
        return "0"
    }
    return timestamp!!
}

fun executeGetUnspent(str: String) {
    var serverID: Int = 1
    var accountID: BigInteger = BigInteger("-1")
    try {
        accountID = str.split(" ")[1].toBigInteger()
        if (str.split(" ").size > 2) {
            serverID = str.split(" ")[2].removePrefix("-s=").toInt()
        }
    } catch (e: Exception) {
        println("PARSING ERROR - please insert the instruction again")
        return
    }
    val restTemplate = RestTemplate()
    val port = (8080 + serverID)
    val resource_uri = "http://localhost:$port/accounts/$accountID/transactions?unspent=true"
    try {
        val utxo_list: List<UTxOWrapper> =
                (restTemplate.getForObject(resource_uri, Array<UTxOWrapper>::class.java)!!).toList()

        for (element in utxo_list) {
            println(element.toString())
        }
    }
    catch(e: HttpClientErrorException){
        println("Error: code " + e.statusCode.toString())
        println(e.message?.split(",")?.get(3)?.removePrefix("\"message\":"))


    }
}
fun executeGetHistory(str: String){
    var serverID: Int = 1
    var limit: Int = -1
    var accountID : BigInteger = BigInteger("-1")
    try {
        accountID = str.split(" ")[1].toBigInteger()
        if (str.split(" ").size > 3) {
            limit = str.split(" ")[2].removePrefix("-lim=").toInt()
            serverID = str.split(" ")[3].removePrefix("-s=").toInt()
        } else {
            if (str.split(" ").size > 2) {
                if (str.split(" ")[2].startsWith("-lim")) {
                    limit = str.split(" ")[2].removePrefix("-lim=").toInt()
                } else {
                    serverID = str.split(" ")[2].removePrefix("-s=").toInt()
                }
            }
        }
    }
    catch(e: Exception){
        println("PARSING ERROR - please insert the instruction again")
        return
    }
    println("IN THE PARSER WE GOT LIMIT $limit")
    val restTemplate = RestTemplate()
    val port = (8080 + serverID)
    val resource_uri = "http://localhost:$port/accounts/$accountID/transactions?limit_=$limit"


//    val gson : Gson = Gson()
//    val response = restTemplate.getForObject(resource_uri, List::class.java)
//    val serializedResponse = gson.toJson(response)
//    val finalResponse = gson.fromJson<>()

    try {
        val history_list: List<TxWrapper> =
                (restTemplate.getForObject(resource_uri, Array<TxWrapper>::class.java))!!.toList()
        for (element in history_list) {
            println(element.toString())
        }
    }
    catch(e: HttpClientErrorException){
        println("Error: code " + e.statusCode.toString())
        println(e.message?.split(",")?.get(3)?.removePrefix("\"message\":"))

    }
}

fun executeGetAllTxs(str: String){
    var serverID : Int =1
    var limit : Int = -1
    try {
        if (str.split(" ").size > 2) {
            limit = str.split(" ")[1].removePrefix("-lim=").toInt()
            serverID = str.split(" ")[2].removePrefix("-s=").toInt()
        } else {
            if (str.split(" ").size > 1) {
                if (str.split(" ")[1].startsWith("-lim")) {
                    limit = str.split(" ")[1].removePrefix("-lim=").toInt()
                } else {
                    serverID = str.split(" ")[1].removePrefix("-s=").toInt()
                }
            }
        }
    }
    catch(e : Exception){
        println("PARSING ERROR - please insert the instruction again")
        return
    }
    val restTemplate = RestTemplate()
    val port = (8080 + serverID)
    val resource_uri = "http://localhost:$port/transactions?limit_=$limit"

    try {
        val history_list: List<TxWrapper> =
                (restTemplate.getForObject(resource_uri, Array<TxWrapper>::class.java))!!.toList()
        for (element in history_list) {
            println(element.toString())
        }
    }
    catch(e: HttpClientErrorException){
        println("Error: code " + e.statusCode.toString())
        println(e.message?.split(",")?.get(3)?.removePrefix("\"message\":"))
    }
}

var noOfServers : Int = 1

suspend fun main() = coroutineScope {
    noOfServers = 1
    println("Welcome to the financial system of the future!")
    println("To use:")
    println("NewClient - create a new client")
    println("GetAllClients - get all client ids")
    println("BuildTx <senderID> {(txID1,address1,amount1,timestamp1),(txID2,address2,amount2,timestamp2),...} {(address1,coins1),(address2,coins2),...} - build a transaction from senderID using money from txIDs to the given addresses")
    println("BuildList {txID1,txID2,...} - build an atomic transaction list using given txIDs")
    println("SendTx <txID> [-s=serverID] - applies the Tx with id txID [ask server s to handle the operation]")
    println("SendTr <senderID> (address,coins) [-s=serverID] - sends said amount of coins to address from senderID [ask server s to handle the operation]")
    println("SendList <senderID> <listID> [-s=serverID] - applies the atomic list with id listID [ask server s to handle the operation]")
    println("GetAllTxIds - get all ????????????????????? ")
    println("GetAllAtomicListIds - get all ????????????????")
    println("GetUnspent <accountID> [-s=serverID] - lists all unspent transaction outputs for the given address")
    println("GetHistory <accountID> [-lim=limit] [-s=serverID] - lists the entire transaction history [or last limit transactions] for the given address ordered by the transaction timestamps")
    println("GetAllTxs [-lim=limit] [-s=serverID] - List the entire ledger history [or last limit transactions] since the Genesis UTxO ordered by the transaction timestamps.")


    while(true){
        val stringInput : String = readLine()!!
        if(isNewClient(stringInput))
            executeNewClient()
        if(isGetAllClients(stringInput))
            executeGetAllClients()
        if(isGetAllTxIds(stringInput))
            executeGetAllTxIds()
        if(isGetAllListIds(stringInput))
            executeGetAllListIds()
        if(isBuildTx(stringInput))
            executeBuildTx(stringInput)
        if(isAtomicList(stringInput))
            executeBuildAtomicList(stringInput)
        if(isSendTx(stringInput))
            executeSendTx(stringInput)
        if(isSendTr(stringInput))
            executeSendTr(stringInput)
        if(isSendList(stringInput))
            executeSendList(stringInput)
        if(isGetUnspent(stringInput))
            executeGetUnspent(stringInput)
        if(isGetHistory(stringInput))
            executeGetHistory(stringInput)
        if(isGetAllTxs(stringInput))
            executeGetAllTxs(stringInput)
        if(stringInput=="Example1")
            SendingAList()
        if(stringInput=="Example2")
            SendingAListTwice()
        if(stringInput=="Example3")
            SendingAListWithMultipleSenders()
        if(stringInput=="Example4")
            SendingAlotTrs()
        if(stringInput=="Example5")
            SendingAlotAtomicLists()
        if(stringInput=="Example6")
            SendingTxFromOneToAnother()
        if(stringInput=="Example7")
            SendingDependentList()
        if(stringInput=="Example9")
            SendingUnreceivedTwice1()
        if(stringInput=="Example10")
            SendingUnreceivedTwice2()
        if(stringInput=="Example11")
            SendingUTxOTwice()
        if(stringInput=="Example12")
            SendingListWithUnsentTx()
    }
}

suspend fun LargeCaseWithDependencies(){
//    val zero_tx = executeSendTr("SendTr 0 (12,1000) -s=5")
//    val first_tx = executeSendTr("SendTr 0 (1,1000) -s=1")
//    val second_tx = executeSendTr("SendTr 0 (4,1000) -s=3")
//    val third_tx = executeSendTr("SendTr 1 (4,500) -s=6")
//    val fourth_tx = executeSendTr("SendTr 4 (7,250) -s=4")
//
//    //Shouldn't work because output sum isn't the same as input sum
//    val tx1 = executeBuildTx("BuildTx 1 {(${third_tx.first},1,500,${third_tx.second})} {(5,50),(1,550)}")
//    //Shouldn't work because insufficient funds
//    val tx2 = executeBuildTx("BuildTx 1 {(${third_tx.first},1,700,${third_tx.second})} {(5,50),(1,650)}")
//    //Shouldn't work because uses other's utxo's
//    val tx3 = executeBuildTx("BuildTx 1 {(${third_tx.first},1,1000,${third_tx.second}),(${fourth_tx.first},4,500,${fourth_tx.first})} {(5,50),(1,1450)}")
//    //Should work even if we use a fraction
//    val tx4 = executeBuildTx("BuildTx 1 {(${third_tx.first},1,500,${third_tx.second})} {(5,50),(1,450)}")
//    //Should work - completely valid
//    val tx5 = executeBuildTx("BuildTx 12 {(${zero_tx.first},12,1000,${zero_tx.second})} {(5,50),(1,450),(12,500)}")
//
//    //These are supposed to fail because they include tx1, tx2, tx3
//    val list_tx1 = executeBuildAtomicList("BuildList {$tx1,$tx4}")
//    executeSendList("SendList 6 $list_tx1 -s=3")
//    val list_tx2 = executeBuildAtomicList("BuildList {$tx5,$tx3}")
//    executeSendList("SendList 2 $list_tx2 -s=4")
//    val list_tx3 = executeBuildAtomicList("BuildList {$tx2,$tx3}")
//    executeSendList("SendList 1 $list_tx3 -s=4")
//    val list_tx4 = executeBuildAtomicList("BuildList {$tx2,$tx4,$tx5}")
//    executeSendList("SendList 4 $list_tx4 -s=1")
//
//
//    //Execute the correct list
//    val list_tx5 = executeBuildAtomicList("BuildList {$tx4,$tx5}")
//    executeSendList("SendList 4 $list_tx5 -s=5")
//
//    //This should work
//    val tx6 = executeBuildTx("BuildTx 1 {($tx4,1,450),($tx5,1,450)} {(3,200),(6,300),(1,400)}")
//    //executeSendTx("SendTx $tx6 -s=5")
//    val tx7 = executeBuildTx("BuildTx 12 {($tx5,12,500)} {(3,50),(2,450)}")
//    //executeSendTx("SendTx $tx7 -s=5")
//    val tx8 = executeBuildTx("BuildTx 7 {($fourth_tx,7,250)} {(8,200),(9,50)}")
//    //executeSendTx("SendTx $tx8 -s=5")
//    val list_tx6 = executeBuildAtomicList("BuildList {$tx6,$tx7,$tx8}")
//    executeSendList("SendList 0 $list_tx6 -s=5")
//
//    //This should work
//    val tx9 = executeBuildTx("BuildTx 3 {($tx8,9,50)} {(10,40),(8,10)}")
//    executeSendTx("SendTx $tx9 -s=2")
}


//Interesting cases:

suspend fun SendingAList(){
    //sending a list with one element
    val first_tx_timestamp = executeSendTr("SendTr 0 (1,1000)")
    val second_tx_timestamp = executeSendTr("SendTr 0 (1,1000)")
    val tx1 = executeBuildTx("BuildTx 1 {(${first_tx_timestamp.first},1,1000,${first_tx_timestamp.second})} {(5,50),(1,950)}")
    val tx2 = executeBuildTx("BuildTx 1 {(${second_tx_timestamp.first},1,1000,${second_tx_timestamp.second})} {(5,50),(1,950)}")
    val list_tx = executeBuildAtomicList("BuildList {$tx1,$tx2}")
    executeSendList("SendList 1 $list_tx")
}

suspend fun SendingAListTwice(){
    val first_tx_timestamp = executeSendTr("SendTr 0 (1,1000)")
    val second_tx_timestamp = executeSendTr("SendTr 0 (1,1000)")
    val tx1 = executeBuildTx("BuildTx 1 {(${first_tx_timestamp.first},1,1000,${first_tx_timestamp.second})} {(5,50),(1,950)}")
    val tx2 = executeBuildTx("BuildTx 1 {(${second_tx_timestamp.first},1,1000,${second_tx_timestamp.second})} {(5,50),(1,950)}")
    val list_tx = executeBuildAtomicList("BuildList {$tx1,$tx2}")
    executeSendList("SendList 1 $list_tx")
    executeSendList("SendList 1 $list_tx")
}

suspend fun SendingAListWithMultipleSenders(){
    val first_tx = executeSendTr("SendTr 0 (1,1000)")
    val second_tx = executeSendTr("SendTr 0 (2,1000)")
    val third_tx = executeSendTr("SendTr 0 (3,1000)")
    val forth_tx = executeSendTr("SendTr 0 (4,1000)")
    val tx1 = executeBuildTx("BuildTx 1 {(${first_tx.first},1,1000,${first_tx.second})} {(5,50),(1,950)}")
    val tx2 = executeBuildTx("BuildTx 1 {(${second_tx.first},2,1000,${second_tx.second})} {(5,50),(1,950)}")
    val tx3 = executeBuildTx("BuildTx 1 {(${third_tx.first},3,1000,${third_tx.second})} {(6,50),(1,950)}")
    val tx4 = executeBuildTx("BuildTx 1 {(${forth_tx.first},4,1000,${forth_tx.second})} {(6,50),(1,950)}")
    val list_tx = executeBuildAtomicList("BuildList {$tx1,$tx2,$tx3,$tx4}")
    executeSendList("SendList 1 $list_tx")
}

fun CoroutineScope.SendingAlotTrs()  {
    (1..10).forEach {
        launch{
            val first_tx: Pair<BigInteger,String>
            val timestamp: String
            withContext(Dispatchers.IO) { // Operations that block the current thread should be in a IO context
                first_tx  = runBlocking { executeSendTr("SendTr 0 ($it,1000)") }
            }
            val getter = it + 10
            val tx1 = executeBuildTx("BuildTx 1 {(${first_tx.first},$it,1000,${first_tx.second})} {($getter,50),($it,950)}")
            executeSendTx("SendTx $tx1")
        }
    }
}

fun CoroutineScope.SendingAlotAtomicLists()  { // all from the same sender
    (1..4).forEach {
        launch{
            val receiver = it + 5
            val first_tx = executeSendTr("SendTr 0 ($it,1000)")
            val second_tx = executeSendTr("SendTr 0 ($it,1000)")
            val tx1 = executeBuildTx("BuildTx $it {(${first_tx.first},$it,1000,${first_tx.second})} {($receiver,50),($it,950)}")
            val tx2 = executeBuildTx("BuildTx $it {(${second_tx.first},$it,1000,${second_tx.second})} {($receiver,50),($it,950)}")
            val list_tx = executeBuildAtomicList("BuildList {$tx1,$tx2}")
            executeSendList("SendList $it $list_tx")
//            executeSendList("SendList $it $list_tx")
        }
    }
}

fun SendingTxFromOneToAnother()  { // all from the same sender
    (1..5).forEach {
        val prev = 2*(it-1)
        val next = 2*it
        println("SendTr $prev ($next,1000)")
        runBlocking{executeSendTr("SendTr $prev ($next,1000)")}
    }
}


suspend fun SendingDependentList(){
    val first_tx = executeSendTr("sendtr 0 (1,1000)")
    val second_tx = executeBuildTx("BuildTx 1 {(${first_tx.first},1,1000,${first_tx.second})} {(2,50),(1,950)}")
    val third_tx = executeBuildTx("BuildTx 2 {($second_tx,2,50,7)} {(3,50)}") // I just put random timestamp
    val list_tx = executeBuildAtomicList("BuildList {$second_tx,$third_tx}")
    executeSendList("SendList 1 $list_tx")
}
suspend fun SendingUnreceivedTwice1(){
    println("Sending twice without ever sending the real tx")
    val pre_tx = executeSendTr("sendtr 0 (2,1000)")
    val unsent_tx = executeBuildTx("BuildTx 2 {(${pre_tx.first},2,1000,${pre_tx.second})} {(3,50),(2,950)}")
    print(pre_tx.second)
    val tx1 = executeBuildTx("BuildTx 2 {($unsent_tx,3,50,${pre_tx.second+2})} {(6,50)")
    executeSendTx("SendTx $tx1")
    val tx2 = executeBuildTx("BuildTx 2 {($unsent_tx,3,50,${pre_tx.second+2})} {(5,50)}")
    executeSendTx("SendTx $tx2")
}

suspend fun SendingUnreceivedTwice2(){
    println("Sending twice with sending the real tx after the first time")
    val pre_tx = executeSendTr("sendtr 0 (2,1000)")
    val unsent_tx = executeBuildTx("BuildTx 2 {(${pre_tx.first},2,1000,${pre_tx.second})} {(3,50),(2,950)}")
    print(pre_tx.second)
    val tx1 = executeBuildTx("BuildTx 2 {($unsent_tx,3,50,${pre_tx.second+2})} {(6,50)")
    executeSendTx("SendTx $tx1")
    executeSendTx("SendTx $unsent_tx")
    val tx2 = executeBuildTx("BuildTx 2 {($unsent_tx,3,50,${pre_tx.second+2})} {(5,50)}")
    executeSendTx("SendTx $tx2")
}

suspend fun SendingUTxOTwice(){
    println("Sending twice with sending the real tx first")
    val pre_tx = executeSendTr("sendtr 0 (1,1000)")
    val tx = executeBuildTx("BuildTx 1 {(${pre_tx.first},1,1000,${pre_tx.second})} {(2,50),(1,950)}")
    val timestamp =executeSendTx("SendTx $tx")
    val tx1 = executeBuildTx("BuildTx 1 {($tx,2,50,$timestamp)} {(4,50)")
    executeSendTx("SendTx $tx1")
    val tx2 = executeBuildTx("BuildTx 1 {($tx,2,50,$timestamp)} {(5,50)}")
    executeSendTx("SendTx $tx2")
}

suspend fun SendingListWithUnsentTx(){
    println("The list should pass since it didn't get the tx yet")
    val first_tx = executeSendTr("SendTr 0 (1,1000)")
    val second_tx = executeSendTr("SendTr 0 (2,1000)")
    val unsent_tx = executeBuildTx("SendTx 2 {(${second_tx.first},2,1000,${second_tx.second})} {(1,1000)}")
    val tx1 = executeBuildTx("BuildTx 1 {(${first_tx.first},1,1000,${first_tx.second})} {(5,50),(1,950)}")
    val tx2 = executeBuildTx("BuildTx 1 {($unsent_tx,1,1000,55)} {(5,50),(1,950)}")
    val list_tx = executeBuildAtomicList("BuildList {$tx1,$tx2}")
    executeSendList("SendList 1 $list_tx")
}

//suspend fun SendingListWithUnsentTxAndAgain(){
////    println("The list should pass since it didn't get the tx yet, and then it tries using the utxo again")
////    val first_tx = executeSendTr("SendTr 0 (1,1000)")
////    val second_tx = executeSendTr("SendTr 0 (2,1000)")
////    val unsent_tx = executeBuildTx("SendTx 2 {($second_tx,2,1000)} {(1,1000)}")
////    val tx1 = executeBuildTx("BuildTx 1 {($first_tx,1,1000)} {(5,50),(1,950)}")
////    val tx2 = executeBuildTx("BuildTx 1 {($unsent_tx,1,1000)} {(5,50),(1,950)}")
////    val list_tx = executeBuildAtomicList("BuildList {$tx1,$tx2}")
////    executeSendList("SendList 1 $list_tx")
////    executeSendTx("SendTx $unsent_tx")
////    val tx3 = executeBuildTx("BuildTx 1 {($unsent_tx,1,1000)} {(5,50),(1,950)}")
////    executeSendTx("SendTx $tx3")
//}



fun OpenClient(clients: HashMap<BigInteger,Client>): BigInteger{
//    val keyGen: KeyPairGenerator = KeyPairGenerator.getInstance("RSA")
//    keyGen.initialize(1024)
//    val key: KeyPair  = keyGen.generateKeyPair()
//    val publicKey =
//    clients[BigInteger(key.public.encoded)]

    return BigInteger("5")
}

fun GetAllClients(clients: HashMap<BigInteger,Client>): MutableSet<BigInteger>{

    return clients.keys
}

fun BuildTx(txList: HashMap<Int,Tx>,clientID: BigInteger, inputs: List<UTxO>, outputs: List<Tr>): Int{
    val index = txList.size
    val string : String = clientID.toString() + index.toString()
    val bytesOftx: ByteArray = string.toByteArray()
    val md = MessageDigest.getInstance("MD5")
    val theMD5digest = md.digest(bytesOftx)
    val txID: BigInteger = BigInteger(theMD5digest)
    txList[index] = tx{   this.txId = txID.toString()
        this.inputs += inputs
        this.outputs += outputs
        this.timestamp = BigInteger("0").toString()}
    return index
}

fun GetBuiltTx(txList: HashMap<Int,Tx>){

}