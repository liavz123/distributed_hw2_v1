package multipaxos

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import recursiveCreate
import zookeeper.kotlin.ZKPaths
import zookeeper.kotlin.ZooKeeperKt
import zookeeper.kotlin.ZookeeperKtClient
import zookeeper.kotlin.createflags.Ephemeral
import zookeeper.kotlin.createflags.Persistent
import java.io.PrintWriter
import java.io.StringWriter

class Omega constructor(private val zk: ZooKeeperKt, val shard_no: Int, val my_id: Int) : OmegaFailureDetector<Int>{
    companion object {
        suspend fun make(zk: ZooKeeperKt, shard_no: Int, my_id: Int): Omega {
            recursiveCreate(zk,shard_no)
            return Omega(zk, shard_no, my_id)
        }
    }

    var currentLeader : Int = -1
    override val leader : Int
        get() = runBlocking { updateLeader(); currentLeader }
    var watchers : MutableList<suspend() -> Unit> = mutableListOf()

    var wasRegistered : Boolean = false
    suspend fun register() {
        if(wasRegistered)
            return
        zk.create("/omegas/sh$shard_no/$my_id") {flags = Ephemeral }
        wasRegistered = true
    }

    suspend fun check(): Int {
        try {
            val helper = zk as ZookeeperKtClient
            val realzk = helper.zk
            val x : List<String> = realzk.getChildren("/omegas/sh$shard_no",false)
            val z = x.map{ZKPaths.extractSequentialSuffix(it)!!}
            val seqNos = z.sorted()
            return seqNos[0].toInt();
        }
        catch (e : Exception){
            val sw = StringWriter()
            val pw = PrintWriter(sw)
            e.printStackTrace(pw)
            val sStackTrace = sw.toString() // stack trace as a string
            println(sStackTrace)
            return -1
        }
    }

    suspend fun updateLeader(){
        val newLeader = check()

        if(newLeader!=currentLeader){
            currentLeader = newLeader
            activateWatchers()
        }

    }
    override fun addWatcher(observer: suspend () -> Unit){
        watchers.add(observer)
    }
    suspend fun activateWatchers(){
        println("In activateWatchers")
        for (func in watchers){
            func()
        }
    }
}