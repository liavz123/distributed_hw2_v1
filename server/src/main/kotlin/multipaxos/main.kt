package multipaxos

import Operation
import RestResult
import com.google.protobuf.ByteString
import com.google.protobuf.kotlin.toByteStringUtf8
import io.grpc.ManagedChannelBuilder
import io.grpc.ServerBuilder
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import zookeeper.kotlin.ZooKeeperKt
import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import com.google.gson.Gson
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import servers_in_my_shard
import java.math.BigInteger
import java.util.concurrent.ConcurrentHashMap
import java.util.stream.Collectors


val biSerializerNew = object : ByteStringBiSerializer<Operation>{
    override fun serialize(obj: Operation) : ByteString {
        val serializator = Gson()
        val y = serializator.toJson(obj)
        val x = y.toByteStringUtf8()
        return x
    }
    override fun deserialize(serialization: ByteString): Operation {
        val serializator = Gson()
        val y = serializator.fromJson(serialization.toStringUtf8(),Operation::class.java)
        return y
    }
}

val biSerializerNewList = object : ByteStringBiSerializer<List<Operation>>{
        override fun serialize(obj: List<Operation>) : ByteString {
            val serializator = Gson()
            val y = serializator.toJson(obj)
            val x = y.toByteStringUtf8()
            return x
        }
    override fun deserialize(serialization: ByteString): List<Operation>{
        val serializator = Gson()
        val y = serializator.fromJson(serialization.toStringUtf8(),Array<Operation>::class.java)
        return y.toList()
    }
}

suspend fun ab_main(my_id : Int, no_of_servers: Int, my_shard: Int,
                    zk: ZooKeeperKt, ab_channel : Channel<Operation>, operations_channel: Channel<Operation>,
                    rest_channels: ConcurrentHashMap<BigInteger, Channel<RestResult>>) = coroutineScope {
    // Displays all debug messages from gRPC
    org.apache.log4j.BasicConfigurator.configure()

    val id = 45000 + my_id

    // Init services
    val learnerService = LearnerService(this)
    val acceptorService = AcceptorService(my_id)

    // Build gRPC server
    val server = ServerBuilder.forPort(id)
            .apply {
                if (id > 0) // Apply your own logic: who should be an acceptor
                    addService(acceptorService)
            }
            .apply {
                if (id > 0) // Apply your own logic: who should be a learner
                    addService(learnerService)
            }
            .build()

    // Use the atomic broadcast adapter to use the learner service as an atomic broadcast service
    val atomicBroadcast = object : AtomicBroadcast<Operation>(learnerService, biSerializerNew) {
        // These are dummy implementations
        override suspend fun _send(byteString: ByteString) {}
        override fun _deliver(byteString: ByteString) : List<Operation> {
            val x =  biSerializerNewList(byteString)
            return x
        }

    }

    val chans = servers_in_my_shard(no_of_servers, Math.ceil(no_of_servers / 3.0).toInt(), my_id).stream().map{it + 45000}.collect(Collectors.toList()).associateWith {
        ManagedChannelBuilder.forAddress("localhost", it).usePlaintext().build()!!
    }

    withContext(Dispatchers.IO) { // Operations that block the current thread should be in a IO context
        server.start()
    }

    // Create channels with clients

    /*
     * Don't forget to add the list of learners to the learner service.
     * The learner service is a reliable broadcast service and needs to
     * have a connection with all processes that participate as learners
     */
    learnerService.learnerChannels = chans.filterKeys { it != id }.values.toList()

    /*
     * You Should implement an omega failure detector.
     */
    val omega: OmegaFailureDetector<Int> = Omega.make(zk, my_shard, my_id)
    println("after Omega")


    // Create a proposer, not that the proposers id's id and
    // the acceptors id's must be all unique (they break symmetry)
    val proposer = Proposer(
            id = my_id, omegaFD = omega, scope = this, acceptors = chans,
            thisLearner = learnerService,
    )

    // Starts The proposer
    proposer.start()

    startRecievingMessages(atomicBroadcast,operations_channel)

    // "Key press" barrier so only one propser sends messages
//    if(my_id != 1){
//        withContext(Dispatchers.IO) { // Operations that block the current thread should be in a IO context
//            System.`in`.read()
//        }
//    }
    startGeneratingMessages(id, proposer,ab_channel, rest_channels)
    withContext(Dispatchers.IO) { // Operations that block the current thread should be in a IO context
        server.awaitTermination()
    }
}

private fun CoroutineScope.startGeneratingMessages(
        id: Int,
        proposer: Proposer, ab_channel: Channel<Operation>, rest_channels: ConcurrentHashMap<BigInteger, Channel<RestResult>>
) {
    launch {
        val mutexLock : Mutex = Mutex()
        val opList : MutableList<Operation> = mutableListOf()
        launch{
            while(true){
                val op = ab_channel.receive()
                println("Got Operation from ab_channel, now starting to generate it OP: " + op.toString())
                var alreadyInList : Boolean = false
                mutexLock.withLock {
                    //check if Tx is already inside the list

                    for(list_op in opList){
                        if(list_op.tx_list == op.tx_list){
                            println("AB: ListTx already in batch")
                            alreadyInList = true
                            rest_channels[op.rest_channel_id]?.send(RestResult(false,"We already working on it",null))
                            break

                        }
                    }
                    if(!alreadyInList){
                        opList.add(op)
                    }
                }
            }
        }
        while(true){
            delay(500L)
            if(opList.isNotEmpty()) {
                mutexLock.withLock {
                    println("PROPOSING WITH SIZE " + opList.size)
                    val prop = biSerializerNewList.serialize(opList)
                    proposer.addProposal(prop)
                    opList.clear()
                }
            }
        }
    }
}

private fun CoroutineScope.startRecievingMessages(atomicBroadcast: AtomicBroadcast<Operation>, worker_channel: Channel<Operation>) {
    launch {
        for ((`seq#`, op) in atomicBroadcast.stream) {
            println("Got an operation in atomic broadcast")
            worker_channel.send(op)
        }
    }
}