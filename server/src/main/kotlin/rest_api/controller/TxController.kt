package com.example.api

import TransactionsService.*
import getAccountShard
import getMyShard
import kotlinx.coroutines.runBlocking
import multipaxos.OmegaFailureDetector
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.math.BigInteger
import java.util.stream.Collectors

import kotlinx.serialization.Serializable
import org.springframework.http.ResponseEntity
import java.util.*

/**
 * Controller for REST API endpoints
 */
@RestController
class TxController(    ) {
    //
    private lateinit var stubMap: Map<Int, ServerServicesGrpcKt.ServerServicesCoroutineStub>
    private lateinit var omegaMap: Map<Int, OmegaFailureDetector<Int>>
    private var my_server_id: Int = -9

    @Autowired
    fun fieldGenerator(springboot: SpringBootBoilerplateApplication) {
        my_server_id = SpringBootBoilerplateApplication.my_server_id
        omegaMap = SpringBootBoilerplateApplication.omegaMap
        stubMap = SpringBootBoilerplateApplication.stubMap
    }

    @GetMapping("/accounts")
    fun createTxFromTx(): Int = omegaMap.size


    @GetMapping("/test")
    @ResponseBody
    fun test(): UTxOWrapper = UTxOWrapper("0", "0", 50, "0")




    @PutMapping("/accounts/{id}/transactions/{txid}")
    fun createTxFromTx(
            @PathVariable("id") accountID: java.math.BigInteger,
            @PathVariable("txid") txID: java.math.BigInteger,
            @RequestBody payloadWrap: TxWrapper
    ): String {
        //Check whether the id of the utxo's is coherent and matches the sender
        var coherentUTxO : Boolean = true
        for (utxo : UTxOWrapper in payloadWrap.inputs){
            if(!utxo.address.equals(accountID.toString())){
                coherentUTxO = false
                break
            }
        }
        if(!coherentUTxO){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "UTxO's address doesn't match sender!")
        }
        val payload: Tx = tx {
            this.txId = payloadWrap.txID
            this.inputs += payloadWrap.inputs.map {
                uTxO {
                    this.txId = it.txId
                    this.address = it.address
                    this.amount = it.amount
                    this.timestamp = it.timestamp
                }
            }
            this.outputs += payloadWrap.outputs.map {
                tr {
                    this.address = it.address
                    this.amount = it.amount
                }
            }
            this.timestamp = payloadWrap.timestamp
        }


        val txShard = getAccountShard(accountID, omegaMap.size)
        var omegaInShard = omegaMap[txShard]?.leader
        val finalL = createTxFromListRequest {
            list = txList {
                list += mutableListOf<Tx>(payload)
            }
        }

        var response: CreateTxFromListResponse? = null
        var failed = false
        while (true) {
            try {
                response = runBlocking { stubMap[omegaInShard]?.createTxFromList(finalL) }
                failed = false
            } catch (e: Exception) {
                failed = true
            }

            if (!failed && response != null) {
                break;
            }
            omegaInShard = omegaMap[txShard]?.leader
        }
        if (response!!.success == false) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, response.msg)
        }
        return response.msg
    }




    @PostMapping("/transactions")
    @ResponseBody
    fun createTxFromList(
            @RequestBody payloadWrap: Pair<TrForTxWrapper?, List<TxWrapper>?>
    ): String{

        var coherentListConditionPerTx: Boolean = true
        for (txx in payloadWrap.second!!) {
            val currentSender: String = txx.inputs[0].address
            for (utxo in txx.inputs) {
                if (!utxo.address.equals(currentSender)) {
                    coherentListConditionPerTx = false
                    break
                }
            }
        }
        if (!coherentListConditionPerTx) {
            throw ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "There is a tx in the list where UTxO's address doesn't match sender!"
            )
        }
        var coherentListNoDependencies: Boolean = true
        val txIDsInList: MutableList<BigInteger> = mutableListOf()
        for (txx in payloadWrap.second!!) {
            txIDsInList.add(txx.txID.toBigInteger())
        }
        for (txx in payloadWrap.second!!) {
            for (utxo in txx.inputs) {
                if (txIDsInList.contains(utxo.txId.toBigInteger())) {
                    coherentListNoDependencies = false
                }
            }
        }

        if (!coherentListNoDependencies) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "There is a dependency in the list!")
        }

        val payload: Pair<TrForTx?, List<Tx>?> = Pair(null, payloadWrap.second?.map {
            tx {
                this.txId = it.txID
                this.timestamp = it.timestamp
                this.inputs += it.inputs.map {
                    uTxO {
                        this.txId = it.txId
                        this.address = it.address
                        this.amount = it.amount
                        this.timestamp = it.timestamp
                    }
                }
                this.outputs += it.outputs.map {
                    tr {
                        this.address = it.address
                        this.amount = it.amount
                    }
                }
            }
        })
        //Get shard of an involved account
        val txShard = getAccountShard(BigInteger(payload.second?.get(0)!!.inputsList[0].address), omegaMap.size)


        var omegaInShard = omegaMap[txShard]?.leader

        var response: CreateTxFromListResponse? = null
        var failed = false

        val grpcList = createTxFromListRequest {
            this.list = txList {
                this.list += payload.second!!
            }
        }

        while (true) {
            try {
                response = runBlocking { stubMap[omegaInShard]?.createTxFromList(grpcList) }
                failed = false
            } catch (e: Exception) {
                failed = true
            }

            if (!failed && response != null) {
                break;
            }
            omegaInShard = omegaMap[txShard]?.leader
        }
        if (response!!.success == false) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, response.msg)
        }
        return response.msg
    }



    @PostMapping("/accounts/{id}/transactions")
    @ResponseBody
    fun createTxFromTrOrList(
            @PathVariable("id") accountID: java.math.BigInteger,
            @RequestBody payloadWrap: Pair<TrForTxWrapper?, List<TxWrapper>?>
    ): String {


        // Got Transfer

        val payload: Pair<TrForTx?, List<Tx>?> = Pair(trForTx {
            this.txId = payloadWrap.first!!.txID
            this.address = payloadWrap.first!!.address
            this.amount = payloadWrap.first!!.amount
        }, null)

        val txShard = getAccountShard(accountID, omegaMap.size)
        var omegaInShard = omegaMap[txShard]?.leader

        var response: CreateTxFromTrResponse? = null

        val finalL = createTxFromTrRequest {
            id = accountID.toString()
            tr = trForTx {
                this.address = payload.first!!.address
                this.amount = payload.first!!.amount
                this.txId = payload.first!!.txId
            }
        }

        var failed = false
        while (true) {
            try {
                response = runBlocking { stubMap[omegaInShard]?.createTxFromTr(finalL) }
                failed = false
            } catch (e: Exception) {
                failed = true
            }

            if (!failed && response != null) {
                break;
            }
            omegaInShard = omegaMap[txShard]?.leader
        }
        if (response!!.success == false) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, response.msg)
        }
        println("REST - returning timestamp - " + response.msg)
        return response.msg
    }


    @GetMapping("/accounts/{id}/transactions")
    fun getAccountUnspent(
            @PathVariable("id") accountID: java.math.BigInteger,
            @RequestParam(defaultValue = "false") unspent: Boolean,
            @RequestParam(defaultValue = "-1") limit_: Int
    ): Any {
        val shard = getAccountShard(accountID, omegaMap.size)
        if (unspent) {
            val grpcMsg = getAccountUnspentTransactionsRequest {
                id = accountID.toString()
            }

            if (shard == getMyShard(my_server_id)) {
                val x = runBlocking { stubMap[my_server_id]?.getAccountUnspentTransactions(grpcMsg) }!!
                return x.unspentTxListList.stream()
                        .map { utxo: UTxO -> UTxOWrapper(utxo.txId, utxo.address, utxo.amount, utxo.timestamp) }
                        .collect(Collectors.toList())
            }

            var omegaInShard = omegaMap[shard]?.leader

            var utxoList: GetAccountUnspentTransactionsResponse? = null
            var failed = false
            while (true) {
                try {
                    utxoList = runBlocking { stubMap[omegaInShard]?.getAccountUnspentTransactions(grpcMsg) }
                    failed = false
                } catch (e: Exception) {
                    failed = true
                }

                if (!failed && utxoList != null) {
                    break;
                }
                omegaInShard = omegaMap[shard]?.leader
            }
            return utxoList!!.unspentTxListList.stream()
                    .map { utxo: UTxO -> UTxOWrapper(utxo.txId, utxo.address, utxo.amount, utxo.timestamp) }.collect(Collectors.toList())

        } else {

            val grpcMsg = getAccountTransactionsHistoryRequest {
                id = accountID.toString()
                limit = limit_
            }
            if (shard == getMyShard(my_server_id)) {
                val x = runBlocking { stubMap[my_server_id]?.getAccountTransactionsHistory(grpcMsg) }!!
                return x.historyList.stream().map { tx: Tx ->
                    TxWrapper(tx.txId,
                            tx.timestamp,
                            tx.inputsList.stream().map { utxo: UTxO -> UTxOWrapper(utxo.txId, utxo.address, utxo.amount, utxo.timestamp) }
                                    .collect(Collectors.toList()),
                            tx.outputsList.stream().map { tr: Tr -> TrWrapper(tr.address, tr.amount) }
                                    .collect(Collectors.toList())
                    )
                }.distinct()
                        .collect(Collectors.toList())


            }
            var omegaInShard = omegaMap[shard]?.leader

            var tx_list: GetAccountTransactionsHistoryResponse? = null
            var failed = false
            while (true) {
                try {
                    tx_list = runBlocking { stubMap[omegaInShard]?.getAccountTransactionsHistory(grpcMsg) }
                    failed = false
                } catch (e: Exception) {
                    failed = true
                }

                if (!failed && tx_list != null) {
                    break;
                }
                omegaInShard = omegaMap[shard]?.leader
            }
            return tx_list!!.historyList.stream().map { tx: Tx ->
                TxWrapper(tx.txId,
                        tx.timestamp,
                        tx.inputsList.stream().map { utxo: UTxO -> UTxOWrapper(utxo.txId, utxo.address, utxo.amount, utxo.timestamp) }
                                .collect(Collectors.toList()),
                        tx.outputsList.stream().map { tr: Tr -> TrWrapper(tr.address, tr.amount) }
                                .collect(Collectors.toList())
                )
            }.distinct()
                    .collect(Collectors.toList())
        }
    }

    @GetMapping("/transactions")
    fun getTransactions(@RequestParam(defaultValue = "-1") limit_: Int): List<TxWrapper> {
        val grpc_msg = getTransactionsHistoryRequest {
            limit = limit_
        }
        var omegaInShard = omegaMap[getMyShard(my_server_id)]?.leader
        var failed = false
        var response: GetTransactionsHistoryResponse? = null
        while (true) {
            try {
                response = runBlocking { stubMap[omegaInShard]?.getTransactionsHistory(grpc_msg) }
                failed = false
            } catch (e: Exception) {
                failed = true
            }

            if (!failed && response != null) {
                break;
            }
            omegaInShard = omegaMap[getMyShard(my_server_id)]?.leader
        }
        if (response?.historyList == null) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Timeout")
        }
        val listReversed = response.historyList!!.stream().map { tx: Tx ->
            TxWrapper(tx.txId,
                    tx.timestamp,
                    tx.inputsList.stream().map { utxo: UTxO -> UTxOWrapper(utxo.txId, utxo.address, utxo.amount, utxo.timestamp) }
                            .collect(Collectors.toList()),
                    tx.outputsList.stream().map { tr: Tr -> TrWrapper(tr.address, tr.amount) }.collect(Collectors.toList())
            )
        }.distinct()
                .collect(Collectors.toList())
        Collections.reverse(listReversed)
        return listReversed
    }
}


@Serializable
data class UTxOWrapper(var txId: String, var address: String , var amount: Long, var timestamp: String){
}
@Serializable
data class TrWrapper(val address : String,val amount: Long){
}
@Serializable
data class TxWrapper(val txID : String, val timestamp: String, val inputs: List<UTxOWrapper>, val outputs: List<TrWrapper> ){

}

@Serializable
data class TrForTxWrapper(val txID : String, val address : String, val amount : Long){

}