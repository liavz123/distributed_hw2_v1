package com.example.api

import TransactionsService.ServerServicesGrpcKt
import multipaxos.OmegaFailureDetector
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@SpringBootApplication
class SpringBootBoilerplateApplication(){
	companion object {
		var stubMap: Map<Int, ServerServicesGrpcKt.ServerServicesCoroutineStub> = HashMap()
		var omegaMap : Map<Int, OmegaFailureDetector<Int>> = HashMap()
		var my_server_id = -10
	}
}

@Configuration
 class generatorFields(){

	@Primary
	@Bean
	public fun stubMapGenerator(): SpringBootBoilerplateApplication = SpringBootBoilerplateApplication()
}
//
//fun main(args: Array<String>) {
//	runApplication<SpringBootBoilerplateApplication>(*args)
//}
