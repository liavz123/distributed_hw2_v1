package zookeeper.kotlin

import org.apache.zookeeper.ZooKeeper
import org.apache.zookeeper.data.Stat


interface ZooKeeperKt
    : ZooKeeperCreator, ZooKeeperNamespacer, ZooKeeperChildrenGetter,
    ZooKeeperDeletor, ZooKeeperExistenceChecker {
    override suspend fun usingNamespace(namespace: Path): ZooKeeperKt =
        NamespaceDecorator.make(this, namespace)

    suspend fun getData(path: String,watch: Boolean, stat: Stat): Pair<ByteArray, Stat>
}
