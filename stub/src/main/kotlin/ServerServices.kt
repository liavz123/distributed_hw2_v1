import TransactionsService.*
import kotlinx.coroutines.channels.Channel
import java.math.BigInteger
import java.util.*
import java.util.concurrent.ConcurrentHashMap


class ServerServices(val server_state: ServerData, val ab_channel: Channel<Operation>, val rest_channels : ConcurrentHashMap<BigInteger, Channel<RestResult>>, private val my_server_id: Int,private val port: Int) : ServerServicesGrpcKt.ServerServicesCoroutineImplBase() {
    // We are inside the sequencer server...
    override suspend fun createTxFromTr(request: CreateTxFromTrRequest):  CreateTxFromTrResponse {
        val accountId = BigInteger(request.id)
        val trForTx = request.tr
        val amount = trForTx.amount
        val target_address = BigInteger(trForTx.address)

        var ctr = 10 // we try 10 times and then we abort?
        while(ctr > 0){
            // try to build Tx?
            val utxoList = server_state.getUserUTxOs(accountId)
            var isEnough = false
            var money: Long = 0
            var inputs = mutableListOf<UTxO>()
            for(utxo in utxoList){

                inputs.add(utxo)
                money += utxo.amount
                if(money >= amount){
                    isEnough = true
                    break
                }
            }
            println("input size after build - " + inputs.size.toString())
            if(isEnough == false){ //there are no enough UTxO
                println("There is NOT enough Money in client")
                return createTxFromTrResponse{this.success=false
                                              this.msg = "Not Enough Money"}
            }
            //There are enough UTxO send Tx to queue
            println("There is enough Money in client")
            val outputs = mutableListOf<Tr>(tr{this.address = trForTx.address
                this.amount = trForTx.amount})
            if(money -trForTx.amount > 0){   // returning the leftover money to the account
                outputs.add(tr{this.address = accountId.toString()
                    this.amount = (money -trForTx.amount)})
            }
            val timestamp = "0"
            val tx_id = trForTx.txId
            val tx = tx{this.txId=tx_id
                        this.inputs+=inputs
                        this.outputs+=outputs
                        this.timestamp=timestamp}


            val grpc_request = createTxFromListRequest{this.list=txList{this.list+=mutableListOf<Tx>(tx)}}
            var rest_result = createTxFromList(grpc_request)

            if(rest_result.success){ // Tx succeeded
                println("SendTr success!")
                return createTxFromTrResponse{this.success=true
                    this.msg = rest_result.msg}
            }
            //trying agian...
            ctr--;
        }
        return createTxFromTrResponse{this.success=false
            this.msg = "Transaction Failed, Try Again"} // If we try 10 times and it doesn't work
    }

    // We are inside the sequencer server...
    override suspend fun createTxFromList(request: CreateTxFromListRequest): CreateTxFromListResponse{
        return CreateTxAux(request, true)
    }

    override suspend fun getAccountUnspentTransactions(request: GetAccountUnspentTransactionsRequest): GetAccountUnspentTransactionsResponse{
        println("getAccountUnspentTransactions")
        val accountID = BigInteger(request.id)
        val x = getAccountUnspentTransactionsResponse{
            unspentTxList += server_state.getUserUTxOs(accountID)
        }
        return x

    }

    override suspend fun getAccountTransactionsHistory(request: GetAccountTransactionsHistoryRequest): GetAccountTransactionsHistoryResponse{
        println("10.1")
        val accountID = BigInteger(request.id)
        val limit = request.limit
        println("10.2")
        val x = getAccountTransactionsHistoryResponse{
            history += server_state.getUserTxs(accountID, limit)
        }
        println("10.3")
        return x

    }

    override suspend fun getTransactionsHistory(request: GetTransactionsHistoryRequest): GetTransactionsHistoryResponse{
        return getTransactionsHistoryAux(request, true) //In this case it returns the global transaction history
    }

    override suspend fun receiveGetTransactionHistory(request: GetTransactionsHistoryRequest): GetTransactionsHistoryResponse{
        return getTransactionsHistoryAux(request, false) //In this case it returns the shard's local transaction history
    }

    override suspend fun receiveTxFromList(request: CreateTxFromListRequest): CreateTxFromListResponse{
        println("GRPC - receiveTxFromList")
        return CreateTxAux(request, false)
    }

    suspend fun CreateTxAux(request: CreateTxFromListRequest, is_sent: Boolean): CreateTxFromListResponse{
        val tx_list = request.list.listList

        // drawing a 128bit id for the channel
        val rn = Random();
        val op = Operation( tx_list, is_sent, BigInteger(128, rn), my_server_id)
        rest_channels[op.rest_channel_id] = Channel<RestResult>(Channel.UNLIMITED)
        ab_channel.send(op)
        val result: RestResult = rest_channels[op.rest_channel_id]!!.receive()
        rest_channels.remove(op.rest_channel_id)


        return createTxFromListResponse{this.success=result.success
            this.msg=result.msg}
    }

    suspend fun getTransactionsHistoryAux(request: GetTransactionsHistoryRequest, is_sent: Boolean): GetTransactionsHistoryResponse{
        val limit = request.limit

        // drawing a 128bit id for the channel
        val rn = Random();
        val op = Operation(mutableListOf<Tx>(), is_sent, BigInteger(128, rn), my_server_id, limit)

        rest_channels[op.rest_channel_id] = Channel<RestResult>(Channel.UNLIMITED)
        ab_channel.send(op)
        val result: RestResult = rest_channels[op.rest_channel_id]!!.receive()
        rest_channels.remove(op.rest_channel_id)

        val return_val = getTransactionsHistoryResponse{this.history += result.history!!}


        return return_val
    }

}