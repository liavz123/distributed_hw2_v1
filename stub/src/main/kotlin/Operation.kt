import TransactionsService.Tx
import java.math.BigInteger

class Operation(val tx_list: List<Tx>, val is_sent: Boolean, val rest_channel_id: BigInteger, val distributer_server: Int ,val limit: Int = -1) {
}