import TransactionsService.Tx
import TransactionsService.UTxO
import java.math.BigInteger
import java.util.Arrays.stream
import java.util.concurrent.ConcurrentHashMap
import java.util.stream.Collectors


class ServerData() {
    var UTxOHashByTxId: ConcurrentHashMap<BigInteger, MutableList<UTxO>> = ConcurrentHashMap()
    var UTxOHashByAccount: ConcurrentHashMap<BigInteger, MutableList<UTxO>> = ConcurrentHashMap()
    var sentHashByTxId : ConcurrentHashMap<java.math.BigInteger,Tx> = ConcurrentHashMap()
    var receivedHashByTxId : ConcurrentHashMap<java.math.BigInteger,Tx> = ConcurrentHashMap();
    var sentHashByAccount : ConcurrentHashMap<java.math.BigInteger,MutableList<Tx>> = ConcurrentHashMap()
    var receivedHashByAccount : ConcurrentHashMap<java.math.BigInteger,MutableList<Tx>> = ConcurrentHashMap();
    var executedListHash: ConcurrentHashMap<List<Tx>, Int> = ConcurrentHashMap()
    var futureUTxO: ConcurrentHashMap<BigInteger, MutableList<UTxO>> = ConcurrentHashMap() // txID -> list of already used UTxO

    fun isUTxOInFuture(utxo: UTxO): Boolean{
        if(!futureUTxO.containsKey(BigInteger(utxo.txId))){
            return false
        }
        if(utxo in futureUTxO[BigInteger(utxo.txId)]!!){
            return true
        }
        return false
    }
    fun listExecuted(list: List<Tx>): Boolean{
        for(l in executedListHash.keys()){
            if(l.equals(list))
                return true
        }
        return false
    }

    fun getUserUTxOs(accountID : java.math.BigInteger):List<UTxO> {
        if(!UTxOHashByAccount.containsKey(accountID)){
            return ArrayList<UTxO>();
        }
        return UTxOHashByAccount[accountID]!!
    }
    fun getUserTxs(accountID: java.math.BigInteger,limit:Int):List<Tx> {
        println("IN GETUSERTXS WITH LIMIT $limit")
        var x : List<Tx>
        println("100.1")
        if (limit==-1) {
            println("100.2")
            println("IF THE NEXT LINE IS NULL THIS IS VERY BAD")
            println(sentHashByAccount)
            if(!sentHashByAccount.containsKey(accountID) && !receivedHashByAccount.containsKey(accountID)){
                println("GRPC - getHistory 0 case")
                x = ArrayList<Tx>()
            }
            else{
                if(sentHashByAccount.containsKey(accountID) && !receivedHashByAccount.containsKey(accountID)){
                    println("GRPC - getHistory 1 case")
                    val l1 = sentHashByAccount[accountID]
                    x = l1!!.stream().sorted { o1: Tx, o2: Tx -> BigInteger(o2.timestamp).compareTo(BigInteger(o1.timestamp)) }
                        .collect(Collectors.toList());

                }
                else{
                    if(!sentHashByAccount.containsKey(accountID) && receivedHashByAccount.containsKey(accountID)){
                        println("GRPC - getHistory 2 case")
                        val l2 = receivedHashByAccount[accountID]
                        x = l2!!.stream().sorted { o1: Tx, o2: Tx -> BigInteger(o2.timestamp).compareTo(BigInteger(o1.timestamp)) }
                            .collect(Collectors.toList());
                    }
                    else{
                        println("GRPC - getHistory 3 case")
                        val l1 = sentHashByAccount[accountID]
                        val l2 = receivedHashByAccount[accountID]
                        x = java.util.stream.Stream.concat(l1!!.stream(), l2!!.stream())
                            .sorted { o1: Tx, o2: Tx -> BigInteger(o2.timestamp).compareTo(BigInteger(o1.timestamp)) }
                            .collect(Collectors.toList());
                    }
                }
            }

            println("100.3")
            return x
        }
        else {
            println(sentHashByAccount)
            if(!sentHashByAccount.containsKey(accountID) && !receivedHashByAccount.containsKey(accountID)){
                x = ArrayList<Tx>()
            }
            else{
                if(sentHashByAccount.containsKey(accountID) && !receivedHashByAccount.containsKey(accountID)){
                    val l1 = sentHashByAccount[accountID]
                    x = l1!!.stream().sorted { o1: Tx, o2: Tx -> BigInteger(o2.timestamp).compareTo(BigInteger(o1.timestamp)) }.limit(limit.toLong())
                        .collect(Collectors.toList());

                }
                else{
                    if(!sentHashByAccount.containsKey(accountID) && receivedHashByAccount.containsKey(accountID)){
                        val l2 = receivedHashByAccount[accountID]
                        x = l2!!.stream().sorted { o1: Tx, o2: Tx -> BigInteger(o2.timestamp).compareTo(BigInteger(o1.timestamp)) }.limit(limit.toLong())
                            .collect(Collectors.toList());
                    }
                    else{
                        val l1 = sentHashByAccount[accountID]
                        val l2 = receivedHashByAccount[accountID]
                        x = java.util.stream.Stream.concat(l1!!.stream(), l2!!.stream())
                            .sorted { o1: Tx, o2: Tx -> BigInteger(o2.timestamp).compareTo(BigInteger(o1.timestamp)) }.limit(limit.toLong())
                            .collect(Collectors.toList());
                    }
                }
            }

            println("100.3")
            return x
        }
    }

    fun addUTxO(utxo: UTxO){
        val accountID = BigInteger(utxo.address)
        if(BigInteger(utxo.txId) in futureUTxO.keys){ // if alredy used, just throw it and remove from futureUTxO
            if(utxo in futureUTxO[BigInteger(utxo.txId)]!!){
                futureUTxO[BigInteger(utxo.txId)]!!.remove(utxo)
                if(futureUTxO[BigInteger(utxo.txId)]!!.size == 1){
                    futureUTxO.remove(BigInteger(utxo.txId))
                }
                return
            }
        }
        if(UTxOHashByAccount[accountID] == null){
            UTxOHashByAccount[accountID] = ArrayList<UTxO>()
        }
        UTxOHashByAccount[accountID]?.add(utxo)
        if(UTxOHashByTxId[BigInteger(utxo.txId)] == null){
            UTxOHashByTxId[BigInteger(utxo.txId)] = ArrayList<UTxO>()
        }
        UTxOHashByTxId[BigInteger(utxo.txId)]?.add(utxo)
    }
    fun addSentTx(tx: Tx){
        val accountID = BigInteger(tx.inputsList[0].address)
        if(sentHashByAccount[accountID] == null){
            sentHashByAccount[accountID] = ArrayList<Tx>()
        }
        sentHashByAccount[accountID]?.add(tx)
        sentHashByTxId[BigInteger(tx.txId)] = tx
    }
    fun addRecivedTx(tx: Tx, accountID: BigInteger){
        if(receivedHashByAccount[accountID] == null){
            receivedHashByAccount[accountID] = ArrayList<Tx>()
        }
        // should only add once per account
        if(! receivedHashByAccount[accountID]!!.contains(tx)){
            receivedHashByAccount[accountID]?.add(tx)
        }
        // could do it multiple times but that should be okay
        receivedHashByTxId[BigInteger(tx.txId)] = tx
    }

    fun removeUTxO(utxo: UTxO){
        if(BigInteger(utxo.txId) !in receivedHashByTxId.keys && BigInteger(utxo.txId) != BigInteger("0")){
            if(futureUTxO[BigInteger(utxo.txId)] == null){
                futureUTxO[BigInteger(utxo.txId)] = mutableListOf(utxo)
            }
            else{
                futureUTxO[BigInteger(utxo.txId)]!!.add(utxo)
            }
            return
        }
        println("removeUTxO - " + "removing now")
        UTxOHashByTxId[BigInteger(utxo.txId)]?.remove(utxo)
        if(UTxOHashByTxId[BigInteger(utxo.txId)]?.size == 0 ){
            UTxOHashByTxId.remove(BigInteger(utxo.txId))
        }
        UTxOHashByAccount[BigInteger(utxo.address)]?.remove(utxo)
    }

    fun checkUTxOValid(utxo: UTxO): Boolean{
        if(!UTxOHashByTxId.containsKey(BigInteger(utxo.txId))){
            return true
        }

        return !UTxOHashByTxId[BigInteger(utxo.txId)]!!.contains(utxo)
    }

    fun getLocalTxSortedByTimestamp() : MutableList<Tx> {
        return java.util.stream.Stream.concat(sentHashByTxId.values.stream(), receivedHashByTxId.values.stream())
            .sorted { o1: Tx, o2: Tx -> BigInteger(o2.timestamp).compareTo(BigInteger(o1.timestamp)) }.collect(Collectors.toList())
    }

}