import TransactionsService.Tx
//if success = true then msg contains timestamp
data class RestResult(val success: Boolean, val msg: String, val history: List<Tx>? = null){}
